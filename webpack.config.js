const path = require('path');
const nodeExternals = require('webpack-node-externals');

var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  optimization: {
    minimize: false
  },
  entry: {
    server: './src/index.ts'
  },
  resolve: {
    extensions: ['.ts', '.js', '.json']
  },
  target: 'node',
  externals: [nodeExternals({
    whitelist: [
    ]
  })],
  node: {
    __dirname: true
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: 'ts-loader',
      }
    ]
  },
  plugins: [
    
  ]
}
