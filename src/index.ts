import * as express from "express";

import { JsonRpcServer } from './json-rpc-server/json-rpc-server';
import { Auth } from './services/auth';

import { isArray } from 'util';
import * as cookie from 'cookie';

const app: express.Application = express();
const port = 8080;

const jsonRpcServer = new JsonRpcServer(app);

var requestLocal=require('request-local');

app.use(require('request-local/middleware').create());

app.use(function (request, response, next) {
    console.log(request.url);
    let jwt, jwtSource;
    let parsedCookie;
    if (request.headers.cookie) {
        let requestCookie :string = isArray(request.headers.cookie) ? (<any[]>request.headers.cookie)[0] || '' : request.headers.cookie;
        parsedCookie=cookie.parse(requestCookie);

        jwt = parsedCookie.jwt?parsedCookie.jwt:null;
        jwtSource='cookie';

        requestLocal.data.httpReferer=parsedCookie.httpReferer?parsedCookie.httpReferer:null;
    }
    if (jwt) {
        jwt=Auth.__getInstance().__jwtDecode(jwt);
        if (jwt==null) {
            Auth.__getInstance().logout();
        }
        requestLocal.data.jwt=jwt;
    }

    next();
});

jsonRpcServer.config();
jsonRpcServer.routes();

app.listen(port, () => {
    console.log('JSON-RPC server listening on port :', port);
});

