import * as uniqid from 'uniqid';
import * as bcrypt from 'bcryptjs';
import { Service } from './service';

var jwt = require('jwt-simple');
var requestLocal = require('request-local');


export class Auth extends Service {
    private static instance: Auth;
    static __getInstance() {
        if (!Auth.instance) Auth.instance = new Auth();
        return Auth.instance;
    }

    tokenLifeTime = 60 * 60 * 24 * 5; // en sec


    private constructor() {
        super();
    }

    public async get() {
        await this.__isAllowed();
        return {id:1, level:0, firstname:'François-Xavier', lastname:'De Nys'};
    }


    public async regularLogin({ email, password, isAdmin = false }: { email: string, password: string, isAdmin?: boolean }) {
        this.__jwtEncode({ userId: 1, level: 0 });
        return {id:1, level:0, firstname:'François-Xavier', lastname:'De Nys'}
    }

    public async logout() {
        requestLocal.data.newJwt = 'deleted';
        return this.ok();
    }

    public __jwtEncode(payload: { userId: number, level: number, exp?: number, jti?: string }, setHeaderCookie = true) {
        payload.exp = Date.now() + this.tokenLifeTime * 1000;
        if (!payload.jti) payload.jti = uniqid();

        const encodedJwt = jwt.encode(payload, 'hbKtAcHHmneYD3HnmJXT', 'HS256');
        if (setHeaderCookie) {
            requestLocal.data.newJwt = encodedJwt;
        }
        return encodedJwt;
    }

    public __jwtDecode(jwtToken: string) {
        try {
            let decodedJwt = jwt.decode(jwtToken, 'hbKtAcHHmneYD3HnmJXT', false, 'HS256');
            if (decodedJwt.exp > Date.now())
                return decodedJwt;
            else
                return null;
        } catch (e) {
            return null;
        }
    }

    public __isLogged(userId?: number): boolean | number {
        let jwt = requestLocal.data.jwt;
        if ((jwt && userId == jwt.userId) || (!userId && jwt))
            return true;
        else
            return false;
    }

    __isAdmin(): boolean {
        let jwt = requestLocal.data.jwt;
        return (jwt && jwt.level >= 10);
    }

    __isPartner(): boolean {
        let jwt = requestLocal.data.jwt;
        return (jwt && jwt.level >= 5);
    }


    async __isAllowed(userId?: number | number[], useDefaultUserId: boolean = true) {
        const auth: Auth = Auth.__getInstance();
        const user = auth.__getUser();

        if (userId === null && !auth.__isAdmin())
            throw this.autorisationFailed();

        if (userId instanceof Array) {
            for (let id of userId){
                if (user && id == user.userId) {
                    return (id);
                }
            }
            if (user && auth.__isAdmin()) return user.userId;
        }
        else if (userId && ((user && userId == user.userId) || auth.__isAdmin())) {
            return (userId);
        }

        if (!userId && user && useDefaultUserId)
            return (user.userId);
        else {
            throw this.autorisationFailed();
        }
    }

    public __getUser(userId?: number): null | { userId: number, level: number, jti: string } {
        let jwt = requestLocal.data.jwt;
        if ((jwt && userId == jwt.userId) || (!userId && jwt))
            return { userId: jwt.userId, level: jwt.level, jti: jwt.jti };
        else
            return null;
    }


}