export class Service {

    error(code: number, message: string): { error: { code: number, message: string } } {
        return { error: { code, message } };
    }

    formError(formErrors: string[]) {
        return { error: { code: -80, formErrors } };
    }

    autorisationFailed(): { error: { code: number, message: string } } {
        return { error: { code: -99, message: 'AUTHORISATION_FAILED' } };
    }

    sqlError(error: any): { error: { code: number, message: string } } {
        return { error: { code: -98, message: error.message } };
    }

    authentificationFailure(provider?: string): { error: { code: number, message: string } } {
        return { error: { code: -97, message: 'AUTHENTIFICATION_FAILURE' + (provider ? ('.' + provider) : '') } };
    }

    emailNotConfirmed(): { error: { code: number, message: string } } {
        return { error: { code: -96, message: 'EMAIL_NOT_CONFIRMED' } };
    }

    invalidParameters(): { error: { code: number, message: string } } {
        return { error: { code: -95, message: 'INVALID_PARAMETERS' } };
    }

    mangopayError(error: any): { error: { code: number, message: string } } {
        if (typeof error == 'object' && error.Message) {
            return { error: { code: -90, message: error.Message } }
        }
        else if (typeof error == 'object' && error.errors) {
            let message='';
            let first=true;
            for (let err in error.errors) {
                if (!first) message+='<br>';
                message+=err+' : '+error.errors[err];
                first=false;
            }
            return { error: { code: -90, message: message } }
        } else {
            return { error: { code: -90, message: error } };
        }
    }

    uploadError(): { error: { code: number, message: string } } {
        return { error: { code: -50, message: 'UPLOAD_ERROR' } };
    }

    ok(): string {
        return 'OK';
    }

    async isDeleted(data:any){
        if(data && data.deleted == true){
            throw { error: { code: -94, message: 'DELETED_DATA' } };
        }
    }
}