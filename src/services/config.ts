import * as fs from 'fs';

export class Config {
    production:boolean=false;
    mysql: {
        host: string,
        port: string,
        user: string,
        password: string,
        database: string,
        dateStrings: boolean,
        connectionLimit: number,
        timezone: string
    };


    private static instance: Config;
    static __getInstance() {
        if (!Config.instance) Config.instance = new Config();
        return Config.instance;
    }

    private constructor() {
        this.mysql = <any>{};

        try {
            Object.assign(this, JSON.parse(fs.readFileSync('config.json').toString()));
        } catch (e) {
            console.error(e);
        }
    }

    a() {
        console.log(this);
    }
}