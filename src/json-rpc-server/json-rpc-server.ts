import { Auth } from './../services/auth';

import { JsonRpcDescription } from "./json-rpc-description";
import { JsonRpcClientGenerator } from "./json-rpc-client-generator";
import { exportedClasses } from './json-rpc-exported-classes';
import * as bodyParser from 'body-parser';
import * as express from "express";
import * as Promise from 'promise';
import * as cookie from 'cookie';

var counter=0;
var requestLocal=require('request-local');

export class JsonRpcServer {
    jsonRpcDescription: JsonRpcDescription = new JsonRpcDescription();
    JsonRpcClientGenerator: JsonRpcClientGenerator = new JsonRpcClientGenerator();
    // JsonRpcAppGenerator: JsonRpcAppGenerator = new JsonRpcAppGenerator();
    
    auth: Auth = Auth.__getInstance();

    public constructor(private app: express.Application) {
    }


    public config() {
        this.app.use(bodyParser.urlencoded({ limit: '3mb', extended: true }));
        this.app.use(bodyParser.json({ limit: '3mb' }));

        this.app.use(function (req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
            res.header('Access-Control-Allow-Credentials', "true");
            next();
        });

    }

    public routes() {

        this.app.post('/services/*', (request: express.Request, response: express.Response) => this.handleJsonRpcRequest(request, response));
        this.app.get('/services/description', (request: express.Request, response: express.Response) => this.jsonRpcDescription.handleRequest(request, response));
        this.app.get('/services/generate-services-files', (request: express.Request, response: express.Response) => {
            this.JsonRpcClientGenerator.generate();
//            this.jsonRpcDescription.generate();
            response.send('DONE');
        });
        // this.app.get('/services/app-services.ts', (request: express.Request, response: express.Response) => this.JsonRpcAppGenerator.handleRequest(request, response));
        // this.app.get('/services/confirmation/:confirmationCode', (request: express.Request, response: express.Response) => {
        //     let auth: Auth = Auth.__getInstance();
        //     auth.confirm(request.params.confirmationCode).then(token => {
        //         response.redirect('/?token=' + token);
        //     }, error => {
        //         console.error(error);
        //         response.redirect('/');
        //     });

        // });
    }

    private handleJsonRpcRequest(request: express.Request, response: express.Response) {
        let className='', method='', params='';
        requestLocal.data.hostname=request.hostname;
        requestLocal.data.protocol=request.protocol;
        try {
            className = request.url.substr(request.url.lastIndexOf('/') + 1).split('?')[0];
            method = request.body.method;
            params = request.body.params;
        } catch (e) {
            return response.send({ jsonrpc: '2.0', id: request.body.id, error: { code: -32601, message: "Forbidden access" } });
        }

        try {
            if (method.substr(0, 2) != '__' || Auth.__getInstance().__isAdmin()) {
                if ((exportedClasses as any)[className]) {
                    let c = (exportedClasses as any)[className];
                    if (c[method]) {
                        (c[method].apply(c, params) as Promise<any>).then(result => {
                            // console.log('result:', result);
                            if (requestLocal.data.newJwt) {
                                if (requestLocal.data.newJwt=='deleted') {
                                    response.setHeader('Set-Cookie', cookie.serialize('jwt', 'deleted',{
                                        path:'/',
                                        httpOnly: true,
                                        maxAge:0
                                    }));
                                } else {
                                    response.setHeader('Set-Cookie', cookie.serialize('jwt', requestLocal.data.newJwt, {
                                        path:'/',
                                        httpOnly: true,
                                        maxAge: Auth.__getInstance().tokenLifeTime
                                    }));
                                }
                            }                           
                            response.send({ jsonrpc: '2.0', id: request.body.id, result: result })
                        }, error => {
                            // console.log('result:', { jsonrpc: '2.0', id: request.body.id }, error.error ? error : {error:{code:-1000, message: error}});
                            response.send((<any>Object).assign({ jsonrpc: '2.0', id: request.body.id }, error.error ? error : {error:{code:-1000, message: error.message}}));
                        });
                    } else {
                        response.send({ jsonrpc: '2.0', id: request.body.id, error: { code: -32601, message: "Method not found" } });
                    }
                } else {
                    response.send({ jsonrpc: '2.0', id: request.body.id, error: { code: -32601, message: "Class not found" } });
                }
            } else {
                response.send({ jsonrpc: '2.0', id: request.body.id, error: { code: -32601, message: "Forbidden access" } });
            }
        }
        catch (e) {
            console.error(e);
        }
    }

}
