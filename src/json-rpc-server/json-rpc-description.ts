import { Config } from './../services/config';
import { exportedClasses } from './json-rpc-exported-classes';
import * as express from "express";
import * as ts from "typescript";
import * as fs from "fs";

export class JsonRpcDescription {
    program: ts.Program;
    checker: ts.TypeChecker;

    constructor() {
        this.program = ts.createProgram(['./src/json-rpc-server/json-rpc-exported-classes.ts'], { target: ts.ScriptTarget.ES5, module: ts.ModuleKind.CommonJS });
        this.checker = this.program.getTypeChecker();
    }

    //reply=[];
    public handleRequest(request: express.Request, response: express.Response) {
        response.header({'content-type':'text/javascript'});
        response.send(fs.readFileSync('./assets/json-rpc-description.json'));
    }

    public generate() {
        fs.writeFile('./assets/json-rpc-description.json', JSON.stringify(this.getDescription()), (err) => {
            if (err) throw err;
            console.log('The file has been saved!');
        });        
    }

    public getDescription() {
        if (Config.__getInstance().production) {
            return [];

        } else {
            let exportedClassNames: any = {};
            for (let exportedClass in exportedClasses) {
                let exportedClassName = (exportedClasses as any)[exportedClass].constructor.toString().match(/\w+/g)[1];
                exportedClassNames[exportedClassName] = exportedClass;


            }
            /*        for (let exportedClass in exportedClasses) {
                        let exportedClassName=(exportedClasses as any)[exportedClass].constructor.toString().match(/\w+/g)[1];
                        exportedClassNames.push(exportedClassName);           
                    } */

            // this.program = ts.createProgram(['./src/json-rpc-server/json-rpc-exported-classes.ts'], {target: ts.ScriptTarget.ES5, module: ts.ModuleKind.CommonJS});
            // this.checker = this.program.getTypeChecker();

            let classesArray: any[] = [];
            for (const sourceFile of this.program.getSourceFiles()) ts.forEachChild(sourceFile, (classNode) => {
                if (classNode.kind === ts.SyntaxKind.ClassDeclaration && this.isNodeExported(classNode)) {
                    let classSymbol = this.checker.getSymbolAtLocation((classNode as any).name);

                    if (classSymbol && exportedClassNames[classSymbol.name]) {
                        let classObject: any = {};
                        classObject.name = exportedClassNames[classSymbol.name];
                        let methodsArray: any[] = [];
                        classNode.forEachChild(methodNode => {
                            if (methodNode.kind === ts.SyntaxKind.MethodDeclaration) {
                                let methodObject: any = {};
                                let methodSymbol = this.checker.getSymbolAtLocation((methodNode as any).name);
                                if (methodSymbol) {
                                    methodObject.name = methodSymbol.name;
                                    //if (methodObject.name[0]=='_') methodObject.name=methodObject.name.substr(1);


                                    if (methodObject.name != '__getInstance' &&
                                        sourceFile.getFullText()
                                            .substr((methodSymbol.valueDeclaration as any).pos, (methodSymbol.valueDeclaration as any).end - (methodSymbol.valueDeclaration as any).pos)
                                            .trim()
                                            .substr(0, 7) != 'private') {
                                        if (methodSymbol.valueDeclaration) {
                                            methodObject.parameters = sourceFile.getFullText().substr((methodSymbol.valueDeclaration as any).parameters.pos, (methodSymbol.valueDeclaration as any).parameters.end - (methodSymbol.valueDeclaration as any).parameters.pos);
                                        }
                                        methodsArray.push(methodObject);
                                    }
                                }
                            }
                        });
                        classObject.methods = methodsArray;
                        classesArray.push(classObject);
                    }
                }
            });

            return classesArray;
            //response.send(classesArray);
        }
    }

    private getParameters(members: ts.Map<ts.Symbol>, parametersObject: any[], sourceFile: ts.SourceFile) {
        let parameterObject: any = {};
        members.forEach(member => {
            switch ((member.valueDeclaration as any).type.kind) {
                case ts.SyntaxKind.TypeLiteral: // object
                    parameterObject['name'] = member.name;
                    parameterObject['type'] = sourceFile.getFullText().substr((member.valueDeclaration as any).type.pos, (member.valueDeclaration as any).type.end - (member.valueDeclaration as any).type.pos);
                    //this.getParameters((member.valueDeclaration as any).type.symbol.members, subParametersObject, sourceFile);
                    //                    parametersObject[member.name]=subParametersObject;
                    break;
                case ts.SyntaxKind.ArrayType: // array
                    parameterObject['name'] = member.name;
                    parameterObject['type'] = sourceFile.getFullText().substr((member.valueDeclaration as any).type.pos, (member.valueDeclaration as any).type.end - (member.valueDeclaration as any).type.pos);
                    console.log('-------- ArrayType');
                    console.log((member.valueDeclaration as any).type.elementType.kind);
                    //                    parametersObject[member.name]=[];
                    break;
                case ts.SyntaxKind.TupleType: // array
                    parameterObject['name'] = member.name;
                    parameterObject['type'] = sourceFile.getFullText().substr((member.valueDeclaration as any).type.pos, (member.valueDeclaration as any).type.end - (member.valueDeclaration as any).type.pos);
                    console.log('---------TupleType');
                    //                    parametersObject[member.name]='[]';
                    break;

                default:
                    parameterObject['name'] = member.name;
                    parameterObject['type'] = sourceFile.getFullText().substr((member.valueDeclaration as any).type.pos, (member.valueDeclaration as any).type.end - (member.valueDeclaration as any).type.pos);
            }
            parametersObject.push(parameterObject);
            //if (member.valueDeclaration)
        });
    }

    private isNodeExported(node: ts.Node): boolean {
        return (node && (node.flags & ts.NodeFlags.ExportContext) !== 0 || (node && typeof node.parent != 'undefined' && node.parent.kind === ts.SyntaxKind.SourceFile));
    }

}