import { Config } from './../services/config';
import * as express from "express";
import * as ts from "typescript";
import * as fs from "fs";

import { JsonRpcDescription } from "./json-rpc-description";



export class JsonRpcClientGenerator {
    jsonRpcDescription:JsonRpcDescription=new JsonRpcDescription();
    result:string='';

    public generate() {
        if (Config.__getInstance().production) {
            return;
        }

        let description=this.jsonRpcDescription.getDescription();
        this.result=`
        import { Injectable, Inject, Optional } from '@angular/core';
        import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
        import { HttpModule } from '@angular/http';
        import { Events } from './services/events';
        import 'rxjs/add/operator/map';

        var requestLocal=require('request-local');

        var json_rpc_id = 0;

        export type WalletType = 'USER' | 'TEST-DRIVE' | 'CHECK' | 'CARE' | 'PAY';

        export class ServerObject
        {
            constructor(private http: HttpClient, private events: Events, @Optional() @Inject('SMAXCONFIG') private smaxConfig: any) {
            }

            protected c(class_name, method, params) {
                return new Promise<any>( (resolve, reject) => {
                    let jwt;
                    try { jwt=requestLocal.data.jwt; } catch (e) {}
                    let headers=new HttpHeaders();
                    headers.set('Content-Type', 'application/json');
                    let serverUrl='';
                    if (typeof(window)=='undefined') {
                        serverUrl='http://server:8080/services/';
                    }else if(window.location.protocol == "file:"){
                        const baseUrl = this.smaxConfig && this.smaxConfig.server ? this.smaxConfig.server : 'https://www.serenimax.com';
                        serverUrl = baseUrl;
                        if( baseUrl.slice(-1) != '/'){
                            serverUrl +='/';
                        }
                        serverUrl +='services/';
                    } else {
                        serverUrl=window.location.protocol+'//'+window.location.hostname+(window.location.port?':'+window.location.port:'')+'/services/';
                    }
                    this.http.post(serverUrl+class_name+(jwt?'?jwt='+jwt:''),
                              {method:method, params:params, id:json_rpc_id++, jsonrpc:'2.0'},
                              { headers })
                        // .map(response => response.json())
                        .subscribe(
                            (response:any) => {
                                if (typeof(response.result)!='undefined') resolve(response.result);
                                else {
                                    if (response.error.code==-999)
                                        this.events.logout();
                                    reject(response.error);
                                }
                            },
                            error => {
                                console.error("http error : can't reach services server");
                                reject({code:-1, message:"http error : can't reach services server"});
                            }
                        );
                });
            }
        }
        `;

        for (let classe of description) {
            this.result+=`
            export class c${classe.name} extends ServerObject {
                `;
            for (let method of classe.methods) {
                this.result+=`public ${method.name}(${method.parameters}) { return this.c('${classe.name}', '${method.name}', Array.from(arguments)) }
                `;
            }
            this.result+=`}
            `;

        }
        this.result+=`
            @Injectable()
            export class Services {
                constructor(private http: HttpClient, private events:Events, @Optional() @Inject('SMAXCONFIG') private smaxConfig: any){
                    `;

        for (let classe of description) {
            this.result+=`this.${classe.name}=new c${classe.name}(http, events, smaxConfig);
                    `;
        }
        this.result+=`}
                `;

        for (let classe of description) {
        	this.result+=`${classe.name}:c${classe.name};
                `;
        }

        this.result+=`}
            `;


        fs.writeFile('../website/app-services.ts', this.result, (err) => {
            if (err) throw err;
            console.log('The file has been saved!');
        });
    }

}
