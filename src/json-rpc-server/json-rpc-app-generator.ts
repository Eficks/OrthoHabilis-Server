import * as express from "express";
import * as ts from "typescript";
import * as fs from "fs";

import { JsonRpcDescription } from "./json-rpc-description";



export class JsonRpcAppGenerator {
    jsonRpcDescription:JsonRpcDescription=new JsonRpcDescription();
    result:string='';
    
    public handleRequest(request:express.Request, response:express.Response) {
        response.header({'content-type':'text/javascript'});
        //if (this.result) response.send(this.result);

        let description=this.jsonRpcDescription.getDescription();
        this.result=`
        import { Injectable } from '@angular/core';
        import { Auth } from './services/auth';
        import { Http, Headers, RequestOptions } from '@angular/http';
        var json_rpc_id=0;

        export type WalletType = 'USER' | 'TEST-DRIVE' | 'CHECK' | 'CARE' | 'PAY';
        
        export class ServerObject
        {
            constructor(protected http : Http, protected auth:Auth){        
            }

            protected c(class_name, method, params) {
                return new Promise<any>( (resolve, reject) => { 
                    let headers:any = { 'Content-Type': 'application/json'};
                    if (this.auth.getJwtToken()) headers.Authorization= 'Bearer '+this.auth.getJwtToken();
                    let options = new RequestOptions({ headers: new Headers(headers) });
                    let serverUrl='';
                    if (typeof(window)=='undefined') {
                        serverUrl=http://www.serenimax.com/services/;
                    } else {
                        serverUrl=window.location.protocol+'://'+window.location.hostname+(window.location.port?:':'+window.location.port:'')+'/services/';
                    }
                    console.log(serverUrl);
                    this.http.post(serverUrl+class_name, 
                              JSON.stringify({method:method, params:params, id:json_rpc_id++, jsonrpc:'2.0'}),
                              options)
                        .map(response => response.json())
                        .subscribe(
                            response => {
                                if (typeof(response.result)!='undefined') resolve(response.result);
                                else {
/*                                    if (response.error.code=-99)
                                        this.auth.clearJwtToken(); */
                                    reject(response.error);
                                }
                            },
                            error => {
                                console.log('error');
                            }
                        );
                });            
            }
        }
        `;

        for (let classe of description) {
            this.result+=`
            export class c${classe.name} extends ServerObject {
                `;
            for (let method of classe.methods) {
                this.result+=`public ${method.name}(${method.parameters}) { return this.c('${classe.name}', '${method.name}', Array.from(arguments)) }
                `;
            }
            this.result+=`}
            `;

        }
        this.result+=`
            @Injectable()
            export class Services {
                constructor(http : Http, _auth:Auth){
                    `;

        for (let classe of description) {
            this.result+=`this.${classe.name}=new c${classe.name}(http, _auth);
                    `;
        }
        this.result+=`}
                `;

        for (let classe of description) {
        	this.result+=`${classe.name}:c${classe.name};
                `;
        }
        this.result+=`}
            `;

        
        fs.writeFile('../app/src/app/app-services.ts', this.result, (err) => {
            if (err) throw err;
            console.log('The file has been saved inside ionic app!');
        });
        
        response.send(this.result);
    }

}
