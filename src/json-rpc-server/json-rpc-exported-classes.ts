import { Auth } from "../services/auth";

class ExportedClasses{
    auth:Auth                                       = Auth.__getInstance();
};

export var exportedClasses:ExportedClasses=new ExportedClasses;