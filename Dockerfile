# Build image
FROM node:8-slim as builder

RUN apt-get update -y && \
    apt-get install -y python git make \
  && rm -rf /var/lib/apt/lists/* \
	&& yarn global add typescript

WORKDIR /app

COPY package.json ./
COPY package-lock.json ./
RUN npm install

COPY . ./
RUN npm run build:prod


# Target image
FROM node:8-slim
RUN apt-get update -y && rm -rf /var/lib/apt/lists/* 

WORKDIR /app

EXPOSE 8080

COPY --from=builder /app/node_modules /app/node_modules
# COPY --from=builder /app/assets /app/assets
COPY --from=builder /app/dist /app/dist

CMD [ "node", "dist/server.js" ]
